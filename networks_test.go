package main

import (
   "net"
   "testing"
)

func TestUWaterlooIPv4(t *testing.T) {
   expected := "uwaterloo"
   actual := IdentifyNetwork(net.ParseIP("129.97.134.71"))
   if actual != expected {
      t.Fatalf("Expected %s, got %s", expected, actual)
   }
}

func TestUWaterlooIPv6(t *testing.T) {
   expected := "uwaterloo"
   actual := IdentifyNetwork(net.ParseIP("2620:101:f000:4901:c5c::f:1055"))
   if actual != expected {
      t.Fatalf("Expected %s, got %s", expected, actual)
   }
}

func TestOtherIPv4(t *testing.T) {
   expected := "other"
   actual := IdentifyNetwork(net.ParseIP("8.8.8.8"))
   if actual != expected {
      t.Fatalf("Expected %s, got %s", expected, actual)
   }
}

func TestOtherIPv6(t *testing.T) {
   expected := "other"
   actual := IdentifyNetwork(net.ParseIP("2001:4860:4860::8888"))
   if actual != expected {
      t.Fatalf("Expected %s, got %s", expected, actual)
   }
}

func TestIPv4Protocol(t *testing.T) {
   expected := "ipv4"
   actual := IdentifyIPProtocol(net.ParseIP("129.97.134.71"))
   if actual != expected {
      t.Fatalf("Expected %s, got %s", expected, actual)
   }
}

func TestIPv6Protocol(t *testing.T) {
   expected := "ipv6"
   actual := IdentifyIPProtocol(net.ParseIP("2620:101:f000:4901:c5c::f:1055"))
   if actual != expected {
      t.Fatalf("Expected %s, got %s", expected, actual)
   }
}

func TestUnknownProtocol(t *testing.T) {
   expected := "other"
   actual := IdentifyIPProtocol(net.ParseIP("computer"))
   if actual != expected {
      t.Fatalf("Expected %s, got %s", expected, actual)
   }
}

