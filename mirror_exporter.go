package main

import (
   "flag"
   "log"
   "net/http"

   "github.com/prometheus/client_golang/prometheus"
)

const Namespace = "mirror"

func main() {
   // Define command line flags
   var (
      listenAddress = flag.String("net.listen-address", ":2400", "Address to listen on for web and metrics interface")
      metricsPath = flag.String("net.telemetry-path", "/metrics", "Path to expose metrics on")

      nginxAccessLogPath = flag.String("nginx.access-log", "/var/log/nginx/access.log", "Path to nginx access log")
      proftpdTransferLogPath = flag.String("proftpd.transfer-log", "/var/log/proftpd/xferlog", "Path to proftpd transfer log")
   )

   flag.Parse()

   nginxExporter, err := NewNginxExporter(*nginxAccessLogPath)
   if err != nil {
      log.Fatal(err)
   }
   prometheus.MustRegister(nginxExporter)

   proftpdExporter, err := NewProftpdExporter(*proftpdTransferLogPath)
   if err != nil {
      log.Fatal(err)
   }
   prometheus.MustRegister(proftpdExporter)

   go nginxExporter.Monitor()
   go proftpdExporter.Monitor()

   http.Handle(*metricsPath, prometheus.Handler())
   http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
      w.Write([]byte(`<!doctype html>
         <html>
            <head>
               <meta charset="utf-8">
               <title>Mirror Exporter</title>
            </head>
            <body>
               <h1>Mirror Exporter</h1>
               <p><a href="` + *metricsPath + `">Metrics</a></p>
            </body>
         </html>`))
   })

   log.Printf("Exporter started.. Listening on %s, metrics available at %s", *listenAddress, *metricsPath)
   log.Fatal(http.ListenAndServe(*listenAddress, nil))
}
